%% Raw EEG Signal

Fs=512; % Sampling frequency of Mindwave Mobile
FN=Fs/2; % Nyquist frequency
t = 0:1/512:20;
t = t(2:end);
t2 = 4:1/512:20;
t2 = t2(2:end);
set(0, 'DefaultAxesFontSize',11)

% Control data
control_afternoon1 = xlsread('EEG_control_afternoon1',1);
control_afternoon2 = xlsread('EEG_control_afternoon2',1);
control_afternoon3 = xlsread('EEG_control_afternoon3',1);
control_afternoon4 = xlsread('EEG_control_afternoon4',1);
control_afternoon5 = xlsread('EEG_control_afternoon5',1);

control_af1 = [];
control_af2 = [];
control_af3 = [];
control_af4 = [];
control_af5 = [];


for(i=9:40)
    control_af1 = [control_af1 control_afternoon1(i,:)];
    control_af2 = [control_af2 control_afternoon2(i,:)];
    control_af3 = [control_af3 control_afternoon3(i,:)];
    control_af4 = [control_af4 control_afternoon4(i,:)];
    control_af5 = [control_af5 control_afternoon5(i,:)];
end

% Mean Control_af
control_af = mean([control_af1;control_af2;control_af3;control_af4;control_af5]);

% Low-Pass filter Design
fc = 50/FN;  % cut-off frequency
[alphaFilter,a] = butter(2, fc);

% High-Pass filter Design
fc2 = 1;
kaiserHigh = fir1(100,2/Fs*fc2, 'high', kaiser(101,10));

% Plot Filter Design
figure (1)
freqz(alphaFilter,a)
title('Figure 1. Low-Pass Filter Frequency Response')
figure (2)
freqz(kaiserHigh, 1)
title('Figure 2. High-Pass Filter Frequency Response')

% Filter Signal
filtered_control_af = filter(kaiserHigh, 1, filter(alphaFilter,a, control_af1));
figure (3)
subplot(2,1,1)
plot(t2, control_af1)
hold on
plot(t2,filtered_control_af)
legend('Raw', 'Filtered')
xlabel('Freuqency (Hz)')
ylabel('Magnitude')
title('Figure 3. Raw and Filtered Control EEG')

subplot(2,1,2)
plot(abs(fft(control_af1)))
hold on
plot(abs(fft(filtered_control_af)))
legend('Raw fft', 'Filtered fft')
xlabel('Freuqency (Hz)')
ylabel('Magnitude')
title('Figure 4. Raw and Filtered Control EEG fft')
xlim([0,50])
%% Wave Type Filters

% Alpha wave Band-Pass Filter
fc1 = 8/FN;
fc2 = 12/FN;
alphaFilter = fir1(101,[fc1 fc2]);

% Beta wave Band-Pass Filter
fc1 = 12/FN;
fc2 = 30/FN;
betaFilter = fir1(101,[fc1 fc2]);

% Plot Alpha wave Band-Pass Filter
figure (4)
freqz(alphaFilter,1)
title('Figure 5. Frequency Response of Alpha Wave filter')

figure (5)
freqz(betaFilter,1)
title('Figure 6. Frequency Response of Beta Wave filter')

% Filter Signals
controlAlpha_af = filter(alphaFilter, 1, control_af);
controlBeta_af = filter(betaFilter, 1, control_af);
figure (6)
subplot(2,1,1)
plot(t2,controlAlpha_af)
xlim([10,15])
xlabel('Time (s)')
ylabel('Magnitude')
title('Figure 7. Alpha Brain Waves')

subplot(2,1,2)
plot(t2,controlBeta_af)
legend('Alpha wave', 'Beta wave')
title('Figure 8. Beta Brain Waves')
xlabel('Time (s)')
ylabel('Magnitude')
xlim([10,15])

figure (7)
plot(abs(fft(controlAlpha_af)))
hold on
plot(abs(fft(controlBeta_af)))
xlim([0,50])
legend('Alpha wave fft', 'Beta wave fft')
title('Figure 9. FFT plots of alpha and beta waves')

%% Power Density Spectra

figure (7)
N = length(control_af);
xdft = fft(control_af);
xdft = xdft(1:N/2+1);
psdx = (1/(Fs*N)) * abs(xdft).^2;
psdx(2:end-1) = 2*psdx(2:end-1);
freq = 0:Fs/length(control_af):Fs/2;
plot(freq,10*log10(psdx))

hold on

N = length(controlAlpha_af);
xdft = fft(controlAlpha_af);
xdft = xdft(1:N/2+1);
psdx = (1/(Fs*N)) * abs(xdft).^2;
psdx(2:end-1) = 2*psdx(2:end-1);
freq = 0:Fs/length(controlAlpha_af):Fs/2;
plot(freq,10*log10(psdx))

hold on

N = length(controlBeta_af);
xdft = fft(controlBeta_af);
xdft = xdft(1:N/2+1);
psdx = (1/(Fs*N)) * abs(xdft).^2;
psdx(2:end-1) = 2*psdx(2:end-1);
freq = 0:Fs/length(controlBeta_af):Fs/2;
plot(freq,10*log10(psdx))
grid on
title('Figure 10. Power density Spectra')
xlabel('Frequency (Hz)')
ylabel('Power/Frequency (dB/Hz)')
xlim([0,50])
legend('Raw EEG', 'Alpha wave', 'Beta wave')

%% Alpha1, Alpha2, Beta1, Beta2 EEG Power

% Brainwave coefficient in each second.
control_a1_af1 = xlsread('EEG_control_afternoon1',7);
control_a2_af1 = xlsread('EEG_control_afternoon1',8);
control_b1_af1 = xlsread('EEG_control_afternoon1',9);
control_b2_af1 = xlsread('EEG_control_afternoon1',10);

control_a1_af2 = xlsread('EEG_control_afternoon2',7);
control_a2_af2 = xlsread('EEG_control_afternoon2',8);
control_b1_af2 = xlsread('EEG_control_afternoon2',9);
control_b2_af2 = xlsread('EEG_control_afternoon2',10);

control_a1_af3 = xlsread('EEG_control_afternoon2',7);
control_a2_af3 = xlsread('EEG_control_afternoon2',8);
control_b1_af3 = xlsread('EEG_control_afternoon2',9);
control_b2_af3 = xlsread('EEG_control_afternoon2',10);

control_a1_af4 = xlsread('EEG_control_afternoon3',7);
control_a2_af4 = xlsread('EEG_control_afternoon3',8);
control_b1_af4 = xlsread('EEG_control_afternoon3',9);
control_b2_af4 = xlsread('EEG_control_afternoon3',10);

control_a1_af5 = xlsread('EEG_control_afternoon4',7);
control_a2_af5 = xlsread('EEG_control_afternoon4',8);
control_b1_af5 = xlsread('EEG_control_afternoon4',9);
control_b2_af5 = xlsread('EEG_control_afternoon4',10);

control_a1_af = mean([control_a1_af1(:,1)';control_a1_af2(:,1)';control_a1_af3(:,1)';control_a1_af4(:,1)']);
control_a2_af = mean([control_a2_af1(:,1)';control_a2_af2(:,1)';control_a2_af3(:,1)';control_a2_af4(:,1)']);
control_b1_af = mean([control_b1_af1(:,1)';control_b1_af2(:,1)';control_b1_af3(:,1)';control_b1_af4(:,1)']);
control_b2_af = mean([control_b2_af1(:,1)';control_b2_af2(:,1)';control_b2_af3(:,1)';control_b2_af4(:,1)']);

t3 = 0:1/2:20;
t3 = t3(2:end);
figure (8)
plot(t3,control_a1_af)
hold on
plot(t3,control_a2_af)
hold on
plot(t3,control_b1_af)
hold on
plot(t3,control_b2_af)
xlabel('Time (s)')
ylabel('Magnitude')
legend('alpha1', 'alpha2', 'beta1','beta2')
title('Figure 10. Wave Coefficients in control experiment')

%%
% Brainwave coefficient in each second.
memory_a1_af1 = xlsread('EEG_memory_afternoon1',7);
memory_a2_af1 = xlsread('EEG_memory_afternoon1',8);
memory_b1_af1 = xlsread('EEG_memory_afternoon1',9);
memory_b2_af1 = xlsread('EEG_memory_afternoon1',10);

memory_a1_af2 = xlsread('EEG_memory_afternoon2',7);
memory_a2_af2 = xlsread('EEG_memory_afternoon2',8);
memory_b1_af2 = xlsread('EEG_memory_afternoon2',9);
memory_b2_af2 = xlsread('EEG_memory_afternoon2',10);

memory_a1_af3 = xlsread('EEG_memory_afternoon2',7);
memory_a2_af3 = xlsread('EEG_memory_afternoon2',8);
memory_b1_af3 = xlsread('EEG_memory_afternoon2',9);
memory_b2_af3 = xlsread('EEG_memory_afternoon2',10);

memory_a1_af4 = xlsread('EEG_memory_afternoon3',7);
memory_a2_af4 = xlsread('EEG_memory_afternoon3',8);
memory_b1_af4 = xlsread('EEG_memory_afternoon3',9);
memory_b2_af4 = xlsread('EEG_memory_afternoon3',10);

memory_a1_af5 = xlsread('EEG_memory_afternoon4',7);
memory_a2_af5 = xlsread('EEG_memory_afternoon4',8);
memory_b1_af5 = xlsread('EEG_memory_afternoon4',9);
memory_b2_af5 = xlsread('EEG_memory_afternoon4',10);

memory_a1_mn1 = xlsread('EEG_memory_morning1',7);
memory_a2_mn1 = xlsread('EEG_memory_morning1',8);
memory_b1_mn1 = xlsread('EEG_memory_morning1',9);
memory_b2_mn1 = xlsread('EEG_memory_morning1',10);

memory_a1_mn2 = xlsread('EEG_memory_morning2',7);
memory_a2_mn2 = xlsread('EEG_memory_morning2',8);
memory_b1_mn2 = xlsread('EEG_memory_morning2',9);
memory_b2_mn2 = xlsread('EEG_memory_morning2',10);

memory_a1_mn3 = xlsread('EEG_memory_morning2',7);
memory_a2_mn3 = xlsread('EEG_memory_morning2',8);
memory_b1_mn3 = xlsread('EEG_memory_morning2',9);
memory_b2_mn3 = xlsread('EEG_memory_morning2',10);

memory_a1_af = mean([memory_a1_af1(:,1)';memory_a1_af2(:,1)';memory_a1_af3(:,1)';memory_a1_af4(:,1)']);
memory_a2_af = mean([memory_a2_af1(:,1)';memory_a2_af2(:,1)';memory_a2_af3(:,1)';memory_a2_af4(:,1)']);
memory_b1_af = mean([memory_b1_af1(:,1)';memory_b1_af2(:,1)';memory_b1_af3(:,1)';memory_b1_af4(:,1)']);
memory_b2_af = mean([memory_b2_af1(:,1)';memory_b2_af2(:,1)';memory_b2_af3(:,1)';memory_b2_af4(:,1)']);

memory_a1_mn = mean([memory_a1_mn1(:,1)';memory_a1_mn2(:,1)';memory_a1_mn3(:,1)']);
memory_a2_mn = mean([memory_a2_mn1(:,1)';memory_a2_mn2(:,1)';memory_a2_mn3(:,1)']);
memory_b1_mn = mean([memory_b1_mn1(:,1)';memory_b1_mn2(:,1)';memory_b1_mn3(:,1)']);
memory_b2_mn = mean([memory_b2_mn1(:,1)';memory_b2_mn2(:,1)';memory_b2_mn3(:,1)']);

sudoku_a1_af1 = xlsread('EEG_sudoku_afternoon1',7);
sudoku_a2_af1 = xlsread('EEG_sudoku_afternoon1',8);
sudoku_b1_af1 = xlsread('EEG_sudoku_afternoon1',9);
sudoku_b2_af1 = xlsread('EEG_sudoku_afternoon1',10);

sudoku_a1_af2 = xlsread('EEG_sudoku_afternoon2',7);
sudoku_a2_af2 = xlsread('EEG_sudoku_afternoon2',8);
sudoku_b1_af2 = xlsread('EEG_sudoku_afternoon2',9);
sudoku_b2_af2 = xlsread('EEG_sudoku_afternoon2',10);

sudoku_a1_af3 = xlsread('EEG_sudoku_afternoon2',7);
sudoku_a2_af3 = xlsread('EEG_sudoku_afternoon2',8);
sudoku_b1_af3 = xlsread('EEG_sudoku_afternoon2',9);
sudoku_b2_af3 = xlsread('EEG_sudoku_afternoon2',10);

sudoku_a1_af4 = xlsread('EEG_sudoku_afternoon3',7);
sudoku_a2_af4 = xlsread('EEG_sudoku_afternoon3',8);
sudoku_b1_af4 = xlsread('EEG_sudoku_afternoon3',9);
sudoku_b2_af4 = xlsread('EEG_sudoku_afternoon3',10);

sudoku_a1_af5 = xlsread('EEG_sudoku_afternoon4',7);
sudoku_a2_af5 = xlsread('EEG_sudoku_afternoon4',8);
sudoku_b1_af5 = xlsread('EEG_sudoku_afternoon4',9);
sudoku_b2_af5 = xlsread('EEG_sudoku_afternoon4',10);


sudoku_a1_mn1 = xlsread('EEG_sudoku_morning1',7);
sudoku_a2_mn1 = xlsread('EEG_sudoku_morning1',8);
sudoku_b1_mn1 = xlsread('EEG_sudoku_morning1',9);
sudoku_b2_mn1 = xlsread('EEG_sudoku_morning1',10);

sudoku_a1_mn2 = xlsread('EEG_sudoku_morning2',7);
sudoku_a2_mn2 = xlsread('EEG_sudoku_morning2',8);
sudoku_b1_mn2 = xlsread('EEG_sudoku_morning2',9);
sudoku_b2_mn2 = xlsread('EEG_sudoku_morning2',10);

sudoku_a1_af = mean([sudoku_a1_af1(:,1)';sudoku_a1_af2(:,1)';sudoku_a1_af3(:,1)';sudoku_a1_af4(:,1)']);
sudoku_a2_af = mean([sudoku_a2_af1(:,1)';sudoku_a2_af2(:,1)';sudoku_a2_af3(:,1)';sudoku_a2_af4(:,1)']);
sudoku_b1_af = mean([sudoku_b1_af1(:,1)';sudoku_b1_af2(:,1)';sudoku_b1_af3(:,1)';sudoku_b1_af4(:,1)']);
sudoku_b2_af = mean([sudoku_b2_af1(:,1)';sudoku_b2_af2(:,1)';sudoku_b2_af3(:,1)';sudoku_b2_af4(:,1)']);

sudoku_a1_mn = mean([sudoku_a1_mn1(:,1)';sudoku_a1_mn2(:,1)']);
sudoku_a2_mn = mean([sudoku_a2_mn1(:,1)';sudoku_a2_mn2(:,1)']);
sudoku_b1_mn = mean([sudoku_b1_mn1(:,1)';sudoku_b1_mn2(:,1)']);
sudoku_b2_mn = mean([sudoku_b2_mn1(:,1)';sudoku_b2_mn2(:,1)']);

memory_a_af = mean([memory_a1_af;memory_a2_af]);
memory_b_af = mean([memory_b1_af;memory_b2_af]);
memory_a_mn = mean([memory_a1_mn;memory_a2_mn]);
memory_b_mn = mean([memory_b1_mn;memory_b2_mn]);

sudoku_a_af = mean([sudoku_a1_af;sudoku_a2_af]);
sudoku_b_af = mean([sudoku_b1_af;sudoku_b2_af]);
sudoku_a_mn = mean([sudoku_a1_mn;sudoku_a2_mn]);
sudoku_b_mn = mean([sudoku_b1_mn;sudoku_b2_mn]);

%% Comparison of EEG power 

t3 = 0:1/2:20;
t3 = t3(2:end);

figure (9)
subplot(2,1,1)
plot(t3,memory_a_mn)
hold on
plot(t3,memory_a_af)
legend('morning', 'afternoon')
xlabel('Time (s)')
ylabel('Magnitude')
title('Figure 12. Memory Task Alpha Waves comparison plot')

subplot(2,1,2)
plot(t3,sudoku_b_mn)
hold on
plot(t3,sudoku_b_af)
legend('morning', 'afternoon')
xlabel('Time (s)')
ylabel('Magnitude')
title('Figure 113. Problem-Solving Beta Waves comparison plot')
